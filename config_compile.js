(function () {
    'use strict';

    // Compile Provider
    function configCompile ($compileProvider) {

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(whatsapp|https?|ftp|mailto|tel|chrome-extension):/);

    }

    configCompile.$inject =['$compileProvider'];

    angular
        .module('bravoureAngularApp')
        .config(configCompile);

})();
